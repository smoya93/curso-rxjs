import { Observable, Observer } from 'rxjs';

const observer: Observer<any> = {
    next: value => console.log('Siguiente [next]: ', value),
    error: error => console.error('Error [obs]: ', error),
    complete: () => console.info('Completado [obs]')
}

/* const obs$ = Observable.create(); */
const obs$ = new Observable<string>(subs => {
    subs.next('Hola');
    subs.next('Adios');
    subs.next('Hola');
    subs.next('Adios');


    //Forzar el error
/*     const a = undefined;
    a.nombre = 'Pepe'; */

    subs.complete();
    subs.next('Adios Completo');
});

/* obs$.subscribe(
    valor => console.log('next: ', valor),
    error => console.warn('error: ', error),
    () => console.info('Completado')
); */

obs$.subscribe(observer);