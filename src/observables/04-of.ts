import { of } from 'rxjs';

const obs$ = of(...[1,2,3,4,5,6],2,3,4,5);

console.log('Inicio del obs$');

obs$.subscribe({
    next: (value) => { console.log('next', value) },
    error: () => { null },
    complete: () => console.log('terminamos la secuencia')
});

console.log('Fin del obs$');